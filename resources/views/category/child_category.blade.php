<li><a href="{{route('category.show', $category)}}">{{ $child_category->name }}</a> </li>
@if ($child_category->categories)
    <ul>
        @foreach ($child_category->categories as $childCategory)
            @include('category.child_category', ['child_category' => $childCategory])
        @endforeach
    </ul>
@endif