@extends('layouts.layout')

@section('content')
    <h2>Category</h2>
    @if($errors)
        <ul>
            @foreach($errors as $error)
            <li>{{$errors}}</li>
            @endforeach
        </ul>
    @endif
{{--    <div>--}}
{{--        <ul>--}}
{{--            @foreach($categories as $category)--}}
{{--            <li>--}}
{{--                <a href="{{route('category.show', $category)}}">{{$category->name}}</a>--}}
{{--            </li>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--    @foreach($categories as $category)--}}
{{--        @if($category->childrens)--}}
{{--            <div class="menu-sub divany-i-kresla" id="menu-{{$category->id}}" data-category="{{$category->slug}}">--}}
{{--                <button class="btn-close btn-close_sub_menu">--}}
{{--                    <svg><use href="#close"></use></svg>--}}
{{--                </button>--}}
{{--                <div class="menu-sub_title">--}}
{{--                    <a>{{$category->name}}</a>--}}
{{--                </div>--}}

{{--                <ul class="menu-sub_categories">--}}
{{--                    @foreach($category->childrens as $children)--}}
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                <img src="{{$children->image}}" alt="{{$children->name}}">--}}
{{--                                <span>{{$children->name}}</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    @endforeach--}}
    <ul>
        @foreach ($categories as $category)
            <li><a href="{{route('category.show', $category)}}">{{ $category->name }}</a></li>
            <ul>
                @foreach ($category->categories as $childCategory)
                    @include('category.child_category', ['child_category' => $childCategory])
                @endforeach
            </ul>
        @endforeach
    </ul>

@endsection

