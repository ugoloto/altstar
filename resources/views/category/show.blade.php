@extends('layouts.layout')

@section('content')
    <h2>Message</h2>
    @if($errors)
        <ul>
            @foreach($errors as $error)
            <li>{{$errors}}</li>
            @endforeach
        </ul>
    @endif
    <ul>
        @foreach ($categories->product as $product)
            <li><a href="{{route('product.index', $product)}}">{{ $product->model }}</a></li>
        @endforeach
    </ul>


@endsection

