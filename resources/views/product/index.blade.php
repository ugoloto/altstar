@extends('layouts.layout')

@section('content')
    <h2>Category</h2>
    @if($errors)
        <ul>
            @foreach($errors as $error)
                <li>{{$errors}}</li>
            @endforeach
        </ul>
    @endif
    <h3>Model {{$product->model}}</h3>
    <h3>Manufacturer {{$product->manufacturer->name}}</h3>
    <h3>Analogs</h3>
    <ul>
        @foreach($product->originals as $original)
            <li>
                <a href="{{route('product.index', $original)}}">{{$original->model}}</a>
            </li>
        @endforeach

    </ul>
    <h3>Originals</h3>
    <ul>
        @foreach($product->analogs as $analog)
            <li>
                <a href="#">{{$analog->model}}</a>
            </li>
        @endforeach
    </ul>
@endsection

