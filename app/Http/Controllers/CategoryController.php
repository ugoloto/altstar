<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::query()->select(['id', 'name', 'category_id'])->where('category_id', '=', 1)
            ->with('categories:id,name,category_id')
            ->get();
        return view('category.index')->with(['categories'=>$categories]);
    }

    public function show(Category $category)
    {
        $category->load('product');
//        $category->product()->paginate(10);
        return view('category.show')->with(['categories' => $category]);
    }
}
