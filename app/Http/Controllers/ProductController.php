<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Product $product)
    {
        $product->load('manufacturer', 'analogs', 'originals');

        return view('product.index')->with(['product' => $product]);
    }
}
