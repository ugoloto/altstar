<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->morphedByMany(Product::class, 'imageable');
    }

    public function categories()
    {
        return $this->morphedByMany(Category::class, 'imageable');
    }

    public function manufacturers()
    {
        return $this->morphedByMany(Manufacturer::class, 'imageable');
    }

    public function cars()
    {
        return $this->morphedByMany(Car::class, 'imageable');
    }
}
