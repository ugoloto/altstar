<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteApplicabilityToAggregatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('applicability_to_aggregates');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('applicability_to_aggregates', function (Blueprint $table) {
            $table->unsignedBigInteger('aggregate_product_id');
            $table->unsignedBigInteger('spare_part_product_id');
            $table->boolean('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
