<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAggregateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aggregate_product', function (Blueprint $table) {
            $table->unsignedBigInteger('aggregate_product_id');
            $table->unsignedBigInteger('spare_part_product_id');

            $table->foreign('aggregate_product_id')->references('id')->on('products');
            $table->foreign('spare_part_product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aggregate_product');
    }
}
